<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OperationalCostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\OperationalCost::factory(100)->create();
    }
}
