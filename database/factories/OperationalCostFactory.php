<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OperationalCostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words(3, true),
            'price' => $this->faker->randomNumber(5, true),
            'notes' => '-',
            'purchase_date' => '2022-03-01',
        ];
    }
}
