<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OperationalCostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/operational', OperationalCostController::class);

// Route::get('/operational', [OperationalCostController::class, 'index'])->name('operational.index');
// Route::get('/operational/create', [OperationalCostController::class, 'create'])->name('operational.create');
// Route::post('/operational', [OperationalCostController::class, 'store'])->name('operational.store');